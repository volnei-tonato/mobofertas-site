# MobOfertas Site

Projeto com o objetivo de mostrar as ofertas e tablóides locais.

###Tecnologias utilizadas:
* React
* Redux/Toolkit
* Axios

_Para interface foi utilizado material-ui._


O site usa uma api refernte ao projeto: [Mobofertas-api](https://github.com/VolneiTonato/mobofertas-api)



![Site](./assets/site.gif)
